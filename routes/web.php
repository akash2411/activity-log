<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Authentication Routes...
$this->get('/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('/login', 'Auth\LoginController@login');
$this->post('/logout', 'Auth\LoginController@logout')->name('logout');

/*// Registration Routes...
$this->get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('/register', 'Auth\RegisterController@register');*/

// Password Reset Routes...
$this->get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'backend'], function () {
    /* Backend - Add Activity */
    Route::get('/add/activity', 'BackendController@getAddActivity')->name('add-activity');
    Route::post('/add/activity', 'BackendController@postAddActivity');
    /* /Backend - Add Activity */

    /* Backend - Edit Activity */
    Route::get('/edit/activity/{id}', 'BackendController@getEditActivity')->name('edit-activity');
    Route::post('/edit/activity/{id}', 'BackendController@postEditActivity');
    /* /Backend - Edit Activity */

    /* Backend - View Activity */
    Route::get('/view/activity', 'BackendController@getViewActivity')->name('view-activity');
    Route::get('/fetch/data', 'BackendController@addany')->name('fetch-activity');
    /* /Backend - View Activity */
});
