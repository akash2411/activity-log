<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\DataTables\ActivityLogDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class BackendController extends Controller
{
    /* Add User Activity */
    public function getAddActivity() {
        return view('backend.add-activitylog');
    }

    public function postAddActivity(Request $request) {
        $this->validate($request,[
            'activity_log' =>'required',
        ]);

        $activity = new ActivityLog;
        $activity->activity_log = $request['activity_log'];
        $activity->user_id = Auth::id();

        if ($activity->save()) {
            Flash::success('Activity Added Successfully');
        } else {
            Flash::error('Error Adding Activity');
        }
        return redirect()->back();
    }
    /* /Add User Activity */

    /* View User Activity */
    public function getViewActivity(ActivityLogDataTable $dataTable) {
        return $dataTable->render('backend.view-activitylog');
    }
    /* /View User Activity */

    /* Edit User Activity */
    public function getEditActivity($id) {
        if (Auth::user()->role == 'admin') {
            $activity_log = ActivityLog::findorfail($id);
        } else {
            $activity_log = ActivityLog::where('user_id', Auth::id())->findorfail($id);
        }

        return view('backend.edit-activitylog')->with('activity_log', $activity_log);
    }

    public function postEditActivity(Request $request, $id) {
        $this->validate($request,[
            'activity_log' =>'required',
        ]);

        $activity = ActivityLog::findOrfail($id);
        $activity->activity_log = $request['activity_log'];
        if ($activity->save()) {
            Flash::success('Activity Added Successfully');
        } else {
            Flash::error('Error Adding Activity');
        }

        return redirect()->back();
    }
    /* /Edit User Activity */
}
