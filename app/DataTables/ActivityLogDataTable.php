<?php

namespace App\DataTables;

use App\ActivityLog;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;

class ActivityLogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($data) {
                if ($data->user_id == Auth::id() || Auth::user()->role == 'admin') {
                    return '<a href="/backend/edit/activity/'.$data->id.'" class="btn btn-xs btn-danger">Edit</a>';
                } else {
                    return '<a href="#" class="btn btn-xs btn-warning">Not Your Activity Log</a>';
                }
            })
            ->addColumn('username', function ($data) {
                return $data->user->name;
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->toFormattedDateString();
            })
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->toFormattedDateString();
            })

            ->rawColumns(['action', 'activity_log']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $activity_log = ActivityLog::select(array('id','activity_log','created_at', 'updated_at', 'user_id'));
        return $this->applyScopes($activity_log);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'SNo'])
                    ->addColumn(['data' => 'username', 'name' => 'username', 'title' => 'User Name', 'exportable' => false, 'orderable' => false, 'searchable' => false])
                    ->addColumn(['data' => 'activity_log', 'name' => 'activity_log', 'title' => 'Activity Log'])
                    ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'])
                    ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'])
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([ 'dom' => 'Bflrtip',
                        'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
                        'order' => [[0, 'desc']],
                        'responsive' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            //'add your columns',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ActivityLog_' . date('YmdHis');
    }
}
