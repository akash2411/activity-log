## Log the activity of your users

## Description

A Simple Project to log the activities of the users. All the activities will be logged in a db-table. All the Users can see each others activity but only the owner of the activity can edit their activity. The admin can see all the users activities and can edit them also.