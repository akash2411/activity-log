@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row margin-top-30">
            <div class="col-md-8 col-md-offset-2">
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Activity Log</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" action="">
                        {!! csrf_field() !!}
                        <!--Activity Log-->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Activity Log</label>
                                <div class="col-md-6">
                                    <textarea maxlength="500" name="activity_log"  class="form-control" rows="5">{{ $activity_log->activity_log }}</textarea>
                                </div>
                            </div>
                            <!-- /Activity Log-->

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
