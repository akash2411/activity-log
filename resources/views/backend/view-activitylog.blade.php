@extends('layouts.app')

@section('content')

    <div class="container spark-screen">
        <div class="row margin-top-40">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Activity Log's</div>
                    <div class="panel-body table">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,b-1.1.0,b-colvis-1.1.0,b-print-1.1.0,r-2.0.0,se-1.1.0/datatables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
    <script type="text/javascript">
        var oTable = $('#dataTableBuilder').DataTable();
        $('#dataTableBuilder').addClass("table-striped dt-responsive nowrap").width("100%");
    </script>
@endpush
